package com.example.springprofiles;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableConfigurationProperties(Configuration.class)
public class HomeController {

    private Configuration configuration;

    public HomeController(Configuration configuration) {
        this.configuration = configuration;
    }

    @GetMapping("/")
    public Configuration hello() {
        return configuration;
    }
}
