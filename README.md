# Understand spring profiles

If I run : `mvn spring-boot:run -Dspring-boot.run.profiles=aws,datasource`
It will load : 
 - `application.yml` with `aws` profile
 - `application-datasource.yml` without any profile
The result will be :
 - browse http://localhost:8081 : port `8081` is define in `aws` profile in `application.yml`
 - it will return `service-default` values, because it did not specify `aws`
 
If I run : `mvn spring-boot:run -Dspring-boot.run.profiles=datasource,aws`
It will load : 
 - `application.yml` with `aws` profile
 - `application-datasource.yml` with `aws` profile
The result will be :
 - browse http://localhost:8081 : port `8081` is define in `aws` profile in `application.yml`
 - it will return `service-aws` values, because `application-datasource.yml` is now aware of `aws` profile 
 
If I run : `mvn spring-boot:run -Dspring-boot.run.profiles=datasource,aws,alibaba`
It will load : 
 - `application.yml` with `alibaba` profile that overrides `aws` profile
 - `application-datasource.yml` with `alibaba` profile that overrides `aws` profile
The result will be :
 - browse http://localhost:8082 : port `8082` is define in `alibaba` profile in `application.yml`
 - it will return `service-alibaba` values, because `application-datasource.yml` is now aware of `alibaba` profile
 - the `password` value will remain `service-aws` as `alibaba` profile does not override this value
 
